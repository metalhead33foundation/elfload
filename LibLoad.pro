TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
    main.cpp \
    libload.cpp \
    StdStream.cpp

HEADERS += \
    elf.h \
    libload.hpp \
    AbstractFwrite.hpp \
    AbstractFread.hpp \
    StdStream.hpp
