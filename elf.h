#ifndef ELF_H
#define ELF_H

#ifdef __cplusplus
#include <cstdint>
#else
#include <stdint.h>
#endif

// Check windows
#if _WIN32 || _WIN64
#if _WIN64
#define ENV64BIT
#else
#define ENV32BIT
#endif
#endif

// Check GCC
#if __GNUC__
#if __x86_64__ || __ppc64__
#define ENV64BIT
#else
#define ENV32BIT
#endif
#endif

// Define the ABIs
#define ABI_SYSTEM_V 0x00
#define ABI_HP_UX 0x01
#define ABI_NETBSD 0x02
#define ABI_LINUX 0x03
#define ABI_GNU_HURD 0x04
#define ABI_SOLARIS 0x06
#define ABI_AIX 0x07
#define ABI_IRIX 0x08
#define ABI_FREEBSD 0x09
#define ABI_TRU64 0x0A
#define ABI_NOVELL_MODESTO 0x0B
#define ABI_OPENBSD 0x0C
#define ABI_OPENVMS 0x0D
#define ABI_NONSTOP_KERNEL 0x0E
#define ABI_AROS 0x0F
#define ABI_FENIX_OS 0x10
#define ABI_CLOUDABI 0x11
#define ABI_SORTIX 0x53

typedef struct _ELF_IDENT {
	uint32_t EI_MAG; // 0x00 - always supposed to be 0x7F 0x45 0x4C 0x46
	uint8_t EI_CLASS; // 0x04
	uint8_t EI_DATA; // 0x05
	uint8_t EI_VERSION; // 0x06
	uint8_t EI_OSABI; // 0x07
	uint8_t EI_ABIVERSION; // 0x08
	char EI_PAD[7]; // 0x09
} ELF_IDENT;

#define ARCH_NOMACHINE 0x00
#define ARCH_SPARC 0x02
#define ARCH_X86 0x03
#define ARCH_MIPS 0x08
#define ARCH_POWERPC 0x14
#define ARCH_S390 0x16
#define ARCH_ARM 0x28
#define ARCH_SUPERH 0x2A
#define ARCH_IA_64 0x32
#define ARCH_X86_64 0x3E
#define ARCH_AARCH64 0xB7
#define ARCH_RISC_V 0xF3

typedef struct _ELF32_HEADER {
	ELF_IDENT e_ident; // 0x09
	uint16_t e_type; // 0x10
	uint16_t e_machine; // 0x12
	uint32_t e_version; // 0x14
	uint32_t e_entry; // 0x18
	uint32_t e_phoff; // 0x1c
	uint32_t e_shoff; // 0x20
	uint32_t e_flags; // 0x24
	uint16_t e_ehsize; // 0x28
	uint16_t e_phentsize; // 0x2A
	uint16_t e_phnum; // 0x2C
	uint16_t e_shentsize; // 0x2E
	uint16_t e_shnum; // 0x30
	uint16_t e_shstrndx; // 0x32
} ELF32_HEADER; // 0x34

typedef struct _ELF64_HEADER {
	ELF_IDENT e_ident; // 0x09
	uint16_t e_type; // 0x10
	uint16_t e_machine; // 0x12
	uint32_t e_version; // 0x14
	uint64_t e_entry; // 0x18
	uint64_t e_phoff; // 0x20
	uint64_t e_shoff; // 0x28
	uint32_t e_flags; // 0x30
	uint16_t e_ehsize; // 0x34
	uint16_t e_phentsize; // 0x36
	uint16_t e_phnum; // 0x38
	uint16_t e_shentsize; // 0x3A
	uint16_t e_shnum; // 0x3C
	uint16_t e_shstrndx; // 0x3E
} ELF64_HEADER; // 0x40

#define PT_NULL 0x00000000
#define PT_LOAD 0x00000001
#define PT_DYNAMIC 0x00000002
#define PT_INTERP 0x00000003
#define PT_NOTE 0x00000004
#define PT_SHLIB 0x00000005
#define PT_PHDR 0x00000006
#define PT_LOOS 0x60000000
#define PT_HIOS 0x6FFFFFFF
#define PT_LOPROC 0x70000000
#define PT_HIPROC 0x7FFFFFFF

typedef struct _PROGRAM_HEADER_32 {
	uint32_t p_type; // 0x00
	uint32_t p_offset; // 0x04
	uint32_t p_vaddr; // 0x08
	uint32_t p_paddr; // 0x0C
	uint32_t p_filesz; // 0x10
	uint32_t p_memsz; // 0x14
	uint32_t p_flags; // 0x18
	uint32_t p_align; // 0x1C
} PROGRAM_HEADER_32;

typedef struct _PROGRAM_HEADER_64 {
	uint32_t p_type; // 0x00
	uint32_t p_flags; // 0x04
	uint64_t p_offset; // 0x08
	uint64_t p_vaddr; // 0x10
	uint64_t p_paddr; // 0x18
	uint64_t p_filesz; // 0x20
	uint64_t p_memsz; // 0x28
	uint64_t p_align; // 0x30
} PROGRAM_HEADER_64;

#define SHT_NULL 0x0
#define SHT_PROGBITS 0x1
#define SHT_SYMTAB 0x2
#define SHT_STRTAB 0x3
#define SHT_RELA 0x4
#define SHT_HASH 0x5
#define SHT_DYNAMIC 0x6
#define SHT_NOTE 0x7
#define SHT_NOBITS 0x8
#define SHT_REL 0x9
#define SHT_SHLIB 0x0A
#define SHT_DYNSYM 0x0B
#define SHT_INIT_ARRAY 0x0E
#define SHT_FINI_ARRAY 0x0F
#define SHT_PREINIT_ARRAY 0x10
#define SHT_GROUP 0x11
#define SHT_SYMTAB_SHNDX 0x12
#define SHT_NUM 0x13
#define SHT_LOOS 0x60000000

typedef struct _SECTION_HEADER_32 {
	uint32_t sh_name; // 0x00
	uint32_t sh_type; // 0x04
	uint32_t sh_flags; // 0x08
	uint32_t sh_addr; // 0x0C
	uint32_t sh_offset; // 0x10
	uint32_t sh_size; // 0x14
	uint32_t sh_link; // 0x18
	uint32_t sh_info; // 0x1C
	uint32_t sh_addralign; // 0x20
	uint32_t sh_entsize; // 0x24
} SECTION_HEADER_32; // 0x28

typedef struct _SECTION_HEADER_64 {
	uint32_t sh_name; // 0x00
	uint32_t sh_type; // 0x04
	uint64_t sh_flags; // 0x08
	uint64_t sh_addr; // 0x10
	uint64_t sh_offset; // 0x18
	uint64_t sh_size; // 0x20
	uint32_t sh_link; // 0x28
	uint32_t sh_info; // 0x2C
	uint64_t sh_addralign; // 0x30
	uint64_t sh_entsize; // 0x38
} SECTION_HEADER_64; // 0x40

#if defined(ENV64BIT)
typedef ELF64_HEADER ElfHeader;
typedef PROGRAM_HEADER_64 ProgramHeader;
typedef SECTION_HEADER_64 SectionHeader;
#elif defined(ENV32BIT)
typedef ELF32_HEADER ElfHeader;
typedef PROGRAM_HEADER_32 ProgramHeader;
typedef SECTION_HEADER_32 SectionHeader;
#else
#error "Your system is not supported, as it seems to be neither 32-bit nor 64-bit."
#endif
typedef ElfHeader* pElfHeader;
typedef ProgramHeader* pProgramHeader;
typedef SectionHeader* pSectionHeader;

#endif /* ELF_H */
