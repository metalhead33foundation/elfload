#ifndef LIBLOAD_HPP
#define LIBLOAD_HPP
#include "elf.h"
#include "AbstractFread.hpp"
#include <vector>
#include <map>
#include <cstdio>
// typedef std::pair<AbstractFread*,pElfHeader> LOAD_LIB;

struct LOAD_LIB
{
	std::vector<char> buffer;
	pElfHeader elf;
	pProgramHeader phead;
	pSectionHeader shead;
};

void loadElfHeader(AbstractFread* origin, LOAD_LIB* lib);
void calibrateElfHeader(LOAD_LIB* lib);
void debugElfInfo(LOAD_LIB* lib);
void debugEheaderInfo(pElfHeader head);
void debugPheaderInfo(pProgramHeader head);
void debugSheaderInfo(pSectionHeader head);
pProgramHeader getPheaderFromLib(LOAD_LIB* lib, size_t index);
pSectionHeader getSheaderFromLib(LOAD_LIB* lib, size_t index);
void* getSectionData(LOAD_LIB* lib, size_t sindex, size_t dindex=0);


#endif // LIBLOAD_HPP
