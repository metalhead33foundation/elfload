#include "libload.hpp"
void loadElfHeader(AbstractFread* origin, LOAD_LIB* lib)
{
	if(!lib) return;
	if(!origin) return;
	lib->buffer.resize(origin->size());
	origin->read(lib->buffer.data(),origin->size());
	calibrateElfHeader(lib);
}
void calibrateElfHeader(LOAD_LIB* lib)
{
	if(!lib) return;
	lib->elf = reinterpret_cast<pElfHeader>(lib->buffer.data());
	lib->phead = reinterpret_cast<pProgramHeader>(&lib->buffer[lib->elf->e_phoff]);
	lib->shead = reinterpret_cast<pSectionHeader>(&lib->buffer[lib->elf->e_shoff]);
}
pProgramHeader getPheaderFromLib(LOAD_LIB* lib, size_t index)
{
	size_t offset = reinterpret_cast<size_t>(lib->buffer.data()) + lib->elf->e_phoff + (lib->elf->e_phentsize * index);
	return reinterpret_cast<pProgramHeader>(offset);
}
pSectionHeader getSheaderFromLib(LOAD_LIB* lib, size_t index)
{
	size_t offset = reinterpret_cast<size_t>(lib->buffer.data()) + lib->elf->e_shoff + (lib->elf->e_shentsize * index);
	return reinterpret_cast<pSectionHeader>(offset);
}
void* getSectionData(LOAD_LIB* lib, size_t sindex, size_t dindex)
{
	pSectionHeader sect = getSheaderFromLib(lib,sindex);
	size_t offset = reinterpret_cast<size_t>(lib->buffer.data()) + sect->sh_offset + dindex;
	return reinterpret_cast<void*>(offset);
}
void debugElfInfo(LOAD_LIB* lib)
{
	if(!lib) return;
	int i;
	// pSectionHeader strings=getSheaderFromLib(lib,lib->elf->e_shstrndx); // SHT_STRTAB
	printf("HEADER:\n");
	debugEheaderInfo(lib->elf);
	printf("\n\n\nPROGRAMS:\n");
	for(i = 0;i < lib->elf->e_phnum;++i)
	{
		pProgramHeader prog = getPheaderFromLib(lib, i);
		printf("\n\tProgram n.%d\n",i+1);
		debugPheaderInfo(prog);
	}
	printf("\n\n\nSECTIONS:\n");
	for(i = 0;i < lib->elf->e_shnum;++i)
	{
		pSectionHeader sect = getSheaderFromLib(lib, i);
		printf("\n\tSection n.%d - %s\n",i+1,getSectionData(lib,lib->elf->e_shstrndx,sect->sh_name));
		debugSheaderInfo(sect);
	}
}
void debugEheaderInfo(pElfHeader head)
{
	if(!head) return;
	printf("Magic number: %d\n",head->e_ident.EI_MAG);
	printf("Class: ");
	switch(head->e_ident.EI_CLASS)
	{
		case 1:
			printf("32-bit\n");
			break;
		case 2:
			printf("64-bit\n");
			break;
		default:
			printf("Unknown\n");
			break;
	}
	printf("Endianness: ");
	switch(head->e_ident.EI_DATA)
	{
		case 1:
			printf("Little Endian\n");
			break;
		case 2:
			printf("Big Indian\n");
			break;
		default:
			printf("Unknown\n");
			break;
	}
	printf("ABI: ");
	switch(head->e_ident.EI_OSABI)
	{
		case ABI_SYSTEM_V:
			printf("System V ");
			break;
		case ABI_HP_UX:
			printf("HP-UX ");
			break;
		case ABI_NETBSD:
			printf("NetBSD ");
			break;
		case ABI_LINUX:
			printf("Linux ");
			break;
		case ABI_GNU_HURD:
			printf("GNU Hurd ");
			break;
		case ABI_SOLARIS:
			printf("Solaris ");
			break;
		case ABI_AIX:
			printf("AIX ");
			break;
		case ABI_IRIX:
			printf("IRIX ");
			break;
		case ABI_FREEBSD:
			printf("FreeBSD ");
			break;
		case ABI_TRU64:
			printf("Tru64 ");
			break;
		case ABI_NOVELL_MODESTO:
			printf("Novell Modesto ");
			break;
		case ABI_OPENBSD:
			printf("OpenBSD ");
			break;
		case ABI_OPENVMS:
			printf("OpenVMS ");
			break;
		case ABI_NONSTOP_KERNEL:
			printf("NonStop Kernel ");
			break;
		case ABI_FENIX_OS:
			printf("Fenix OS ");
			break;
		case ABI_AROS:
			printf("AROS ");
			break;
		case ABI_CLOUDABI:
			printf("CloudABI ");
			break;
		case ABI_SORTIX:
			printf("Sortix ");
			break;
		default:
			printf("Unknown ABI ");
			break;
	}
	printf("v.%d\n",head->e_ident.EI_ABIVERSION);
	printf("Type: ");
	switch(head->e_type)
	{
	case 1:
		printf("Relocatable\n");
		break;
	case 2:
		printf("Executable\n");
		break;
	case 3:
		printf("Shared\n");
		break;
	case 4:
		printf("Core\n");
		break;
	default:
		printf("Unknown\n");
		break;
	}
	printf("Architecture: ");
	switch(head->e_machine)
	{
	case ARCH_NOMACHINE:
		printf("Unknown\n");
		break;
	case ARCH_SPARC:
		printf("Scalable Processor Architecture\n");
		break;
	case ARCH_X86:
		printf("Intel X86\n");
		break;
	case ARCH_MIPS:
		printf("MIPS\n");
		break;
	case ARCH_POWERPC:
		printf("PowerPC\n");
		break;
	case ARCH_S390:
		printf("z/Architecture S390\n");
		break;
	case ARCH_ARM:
		printf("Advanced RISC Machine\n");
		break;
	case ARCH_SUPERH:
		printf("SuperH\n");
		break;
	case ARCH_IA_64:
		printf("Intel Itanium 64\n");
		break;
	case ARCH_X86_64:
		printf("AMD64\n");
		break;
	case ARCH_AARCH64:
		printf("ARMv8-A\n");
		break;
	case ARCH_RISC_V:
		printf("RISC-V\n");
		break;
	default:
		printf("Unknown\n");
		break;
	}
	printf("ELF version: %d\n",head->e_version);
	printf("Entry point: %p\n",reinterpret_cast<void*>(head->e_entry));
	printf("Program header table offset: %p\n",reinterpret_cast<void*>(head->e_phoff));
	printf("Section header table offset: %p\n",reinterpret_cast<void*>(head->e_shoff));
	printf("Header size: %d\n",head->e_ehsize);
	printf("Size of a program header table entry: %d\n",head->e_phentsize);
	printf("Number of entries in the program header table: %d\n",head->e_phnum);
	printf("Size of a program section table entry: %d\n",head->e_shentsize);
	printf("Number of entries in the section header table: %d\n",head->e_shnum);
	printf("Section names index: %d\n",head->e_shstrndx);
}
void debugPheaderInfo(pProgramHeader head)
{
	printf("Type: ");
	switch(head->p_type)
	{
	case PT_NULL:
		printf("PT_NULL \n");
		break;
	case PT_LOAD:
		printf("PT_LOAD \n");
		break;
	case PT_DYNAMIC:
		printf("PT_DYNAMIC \n");
		break;
	case PT_INTERP:
		printf("PT_INTERP \n");
		break;
	case PT_NOTE:
		printf("PT_NOTE \n");
		break;
	case PT_SHLIB:
		printf("PT_SHLIB \n");
		break;
	case PT_PHDR:
		printf("PT_PHDR \n");
		break;
	case PT_LOOS:
		printf("PT_LOOS \n");
		break;
	case PT_HIOS:
		printf("PT_HIOS \n");
		break;
	case PT_LOPROC:
		printf("PT_LOPROC \n");
		break;
	case PT_HIPROC:
		printf("PT_HIPROC \n");
		break;
	default:
		printf("Unknown \n");
		break;
	}
	printf("Segment offset: %p\n",reinterpret_cast<void*>(head->p_offset));
	printf("Virtual address: %p\n",reinterpret_cast<void*>(head->p_vaddr));
	printf("Physical address: %p\n",reinterpret_cast<void*>(head->p_paddr));
	printf("Segment filesize: %ul\n",head->p_filesz);
	printf("Segment memory size: %ul\n",head->p_memsz);
}
void debugSheaderInfo(pSectionHeader head)
{
	printf("Type: ");
	switch(head->sh_type)
	{
	case SHT_NULL:
	printf("Section header table entry unused\n");
	break;
	case SHT_PROGBITS:
	printf("Program data\n");
	break;
	case SHT_SYMTAB:
	printf("Symbol table\n");
	break;
	case SHT_STRTAB:
	printf("String table\n");
	break;
	case SHT_RELA:
	printf("Relocation entries with addends\n");
	break;
	case SHT_HASH:
	printf("Symbol hash table\n");
	break;
	case SHT_DYNAMIC:
	printf("Dynamic linking information\n");
	break;
	case SHT_NOTE:
	printf("Notes\n");
	break;
	case SHT_NOBITS:
	printf("Program space with no data (bss)\n");
	break;
	case SHT_REL:
	printf("Relocation entries, no addends\n");
	break;
	case SHT_SHLIB:
	printf("Reserved\n");
	break;
	case SHT_DYNSYM:
	printf("Dynamic linker symbol table\n");
	break;
	case SHT_INIT_ARRAY:
	printf("Array of constructors\n");
	break;
	case SHT_FINI_ARRAY:
	printf("Array of destructors\n");
	break;
	case SHT_PREINIT_ARRAY:
	printf("Array of pre-constructors\n");
	break;
	case SHT_GROUP:
	printf("Section group\n");
	break;
	case SHT_SYMTAB_SHNDX:
	printf("Extended section indeces\n");
	break;
	case SHT_NUM:
	printf("Number of defined types.\n");
	break;
	case SHT_LOOS:
	printf("Start OS-specific.\n");
	break;
	default:
	printf("Unknown.\n");
	break;
	}
	printf("Segment offset: %p\n",reinterpret_cast<void*>(head->sh_offset));
	printf("S address: %p\n",reinterpret_cast<void*>(head->sh_addr));
	printf("Segment size: %ul\n",head->sh_size);
	printf("Index: %ul\n",head->sh_link);
	if(head->sh_size && head->sh_entsize)
	{
		size_t ents = head->sh_size / head->sh_entsize;
		printf("Number of entries: %ul\n",ents);
		// size_t i;
	}
}
